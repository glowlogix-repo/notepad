package com.example.usmanayoub.noteapp;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Usman Ayoub on 8/7/2017.
 */

public class UserModel implements Serializable {
    private String title, content;
    private int id;
    private Long dateCreated;
    private Long dateModified;



    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Long getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Long dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Long getDateModified() {
        return dateModified;
    }

    public void setDateModified(Long dateModified) {
        this.dateModified = dateModified;
    }


 //   public String getReadableModifiedDate(){
  //      SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy - h:mm a", Locale.getDefault());
  //      sdf.setTimeZone(TimeZone.getTimeZone("IST"));
  //      Date modifiedDate = getDateModified().getTime();
   //     String displayDate = sdf.format(modifiedDate);
    //    return displayDate;
   // }
}
