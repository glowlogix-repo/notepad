package com.example.usmanayoub.noteapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Usman Ayoub on 8/7/2017.
 */

public class CustomAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<UserModel> userModelArrayList;

    public CustomAdapter(Context context, ArrayList<UserModel> userModelArrayList) {

        this.context = context;
        this.userModelArrayList = userModelArrayList;
    }

    @Override
    public int getCount() {
        return userModelArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return userModelArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.lv_items, null, true);

            holder.tvtitle = (TextView) convertView.findViewById(R.id.text_view_note_title);
            holder.tvdate = (TextView) convertView.findViewById(R.id.text_view_note_date);
            //holder.tvcontent = (TextView) convertView.findViewById(R.id.text_view_note_content);


            convertView.setTag(holder);
        }else {
            // the getTag returns the viewHolder object set as a tag to the view
            holder = (ViewHolder)convertView.getTag();
        }

        holder.tvtitle.setText(userModelArrayList.get(position).getTitle());
        //holder.tvdate.setText(userModelArrayList.get(position).getReadableModifiedDate());
        //holder.tvcontent.setText("Content: "+userModelArrayList.get(position).getContent());

        return convertView;
    }

    private class ViewHolder {

        protected TextView tvtitle, tvdate;
    }

    //public String getReadableModifiedDate(){
      //  SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy - h:mm a", Locale.getDefault());
       // sdf.setTimeZone(userModelArrayList.getDateModified().getTimeZone());
        //Date modifiedDate = getDataModified().getTime();
        //String displayDate = sdf.format(modifiedDate);
        //return displayDate;
    //}
}
